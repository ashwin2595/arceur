

    <!--hero section-->
    <div  class="parallax text-center vertical-align home-banner">
        <div class="container-mid">
            <div class="container">
                <div class="banner-title light-txt">
                    <h1 class="text-uppercase ls-20">Archetecor</h1>
                    <h3 class="text-uppercase"> A theme Really with massive possibilities</h3>
                </div>
            </div>
        </div>
    </div>
    <!--hero section-->

    <!--body content start-->
    <section class="body-content">

        <!--team member-->
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="m-bot-20 inline-block">
                        <!--title-->
                        <div class="heading-title-alt border-short-bottom text-center">
                            <h3 class="text-uppercase">Our Services</h3>
                            <div class="half-txt">We are a team of multi-skilled and curious digital specialists who are always up for a challenge and learning as fast as digital is changing. We are a team of multi-skilled and curious digital specialists who are always up for a challenge.</div>
                        </div>
                        <!--title-->
                    </div>
                    <div class="col-md-4">
                        <div class="team-member">
                            <div class="team-img">
                                    <img src="img/site/services/shutterstock_392498833.jpg" alt=""/>
                                <div class="team-intro light-txt">
                                    <h5>Martin Smith</h5>
                                    <span>founder & ceo</span>
                                </div>
                            </div>
                            <div class="team-hover">
                                <div class="desk">
                                    <h4>I love my Studio</h4>
                                    <p>We are a team of multi-skilled and curious digital specialists who are always up for a challenge and learning as fast as digital is changing.</p>
                                </div>
                                <div class="s-link">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="team-member">
                            <div class="team-img">
                                <img src="img/site/services/sx8ewpollcckwslveux3.jpg" alt=""/>
                                <div class="team-intro light-txt">
                                    <h5>Franklin Harbet</h5>
                                    <span>HR Manager</span>
                                </div>
                            </div>
                            <div class="team-hover">
                                <div class="desk">
                                    <h4>Connecting People</h4>
                                    <p>We are a team of multi-skilled and curious digital specialists who are always up for a challenge and learning as fast as digital is changing.</p>
                                </div>
                                <div class="s-link">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="team-member">
                            <div class="team-img">
                                <img src="img/site/services/barandbench_2020-03_b8b8d14c-f8d1-48e5-8aee-cdb702f09b3c_architects.webp" alt=""/>
                                <div class="team-intro light-txt">
                                    <h5>Linda Anderson</h5>
                                    <span>Marketing Manager</span>
                                </div>
                            </div>
                            <div class="team-hover">
                                <div class="desk">
                                    <h4>Network Builder</h4>
                                    <p>We are a team of multi-skilled and curious digital specialists who are always up for a challenge and learning as fast as digital is changing.</p>
                                </div>
                                <div class="s-link">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--team member-->     

        <!--blog-->
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="inline-block">
                        <!--title-->
                        <div class="heading-title-alt border-short-bottom text-center">
                            <h3 class="text-uppercase">SEO Talks</h3>
                            <!-- <div class="half-txt">Sed sit amet iaculis nisi. Mauris ridiculus elementum non felis etewe blandit Pellentesque eu quam sem, ac malesuada leo sem quam pellente.</div> -->
                        </div>
                        <!--title-->
                    </div>


                    <!--blog post-->
                    <div class="post-list-aside">
                        <div class="post-single">
                            <div class="col-md-6">
                                <div class="post-img title-img">
                                    <img src="img/site/seo/professional-workers-500x500.jpg" alt="">
                                    <div class="info">We work together for fun</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="post-desk">
                                    <ul class="post-cat">
                                        <li><a href="#">interface</a></li>
                                        <li><a href="#">standard</a></li>
                                    </ul>
                                    <h4 class="text-uppercase">
                                        <a href="#">we work together for fun</a>
                                    </h4>
                                    <div class="date">
                                        <a href="#" class="author">martin smith</a> july 29, 2015
                                    </div>
                                    <p>
                                        Phasellus fringilla suscipit risus nec eleifend. Pellentesque eu quam sem, ac malesuada leo sem quam pellente. Awesome sliders give you the opportunity to showcase your content.
                                        fringilla suscipit risus nec eleifend. Pellentesque eu quam sem, ac malesuada leo sem quam pellente. Awesome sliders give you the opportunity to showcase your content.
                                    </p>
                                    <a href="#" class="p-read-more">Read More <i class="icon-arrows_slim_right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--blog post-->


                </div>
            </div>
        </div>
        <!--blog-->

        <!--tabs-->
        <div class="page-content tab-parallax">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <!--tabs square start-->
                        <section class="icon-box-tabs ">
                            <ul class="nav nav-pills">
                                <li class="active">
                                    <a data-toggle="tab" href="#tab-15" style="background-color: #ffe2e2">
                                        <i class="icon-mobile"> </i>
                                    </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#tab-16" style="background-color: #f1fdd0">
                                        <i class="icon-documents"></i>
                                    </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#tab-17" style="background-color: #c4e8f3">
                                        <i class="icon-lightbulb"></i>
                                    </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#tab-18" style="background-color: #ffc6e5">
                                        <i class="icon-circle-compass"></i>
                                    </a>
                                </li>

                                <li class="">
                                    <a data-toggle="tab" href="#tab-19" style="background-color: #f3e0bc">
                                        <i class="icon-telescope"></i>
                                    </a>
                                </li>

                            </ul>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div id="tab-15" class="tab-pane active">
                                        <div class="heading-title-alt">
                                            <span class="heading-sub-title-alt text-uppercase theme-color-">full responsive</span>
                                            <h2 class="text-uppercase">we work together for fun</h2>
                                        </div>
                                        In quis scelerisque velit. Proin pellentesque neque ut scelerisque dapibus. Praesent elementum feugiat dignissim. Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque mattis, leo quam aliquet diam, congue laoreet elit metus eget diam. Proin ac metus diam. In quis scelerisque velit. Proin pellentesque neque ut scelerisque dapibus. Praesent elementum feugiat dignissim. Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque mattis, leo quam aliquet diam, congue laoreet elit metus eget diam. Proin ac metus diam.
                                    </div>
                                    <div id="tab-16" class="tab-pane">
                                        <div class="heading-title-alt">
                                            <span class="heading-sub-title-alt text-uppercase theme-color-">work for fun</span>
                                            <h2 class="text-uppercase">Massive UI collection</h2>
                                        </div>
                                        Leo quam aliquet diam, congue laoreet elit metus eget diam. Proin ac metus diam. In quis scelerisque velit. Proin pellentesque neque ut scelerisque dapibus. Praesent elementum feugiat dignissim. Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque mattis, leo quam aliquet diam, congue laoreet elit metus eget diam. Proin ac metus diam. In quis scelerisque velit. Proin pellentesque neque ut scelerisque dapibus. Praesent elementum feugiat dignissim. Nunc placerat mi id nisi interdum mollis.
                                    </div>
                                    <div id="tab-17" class="tab-pane">
                                        <div class="heading-title-alt">
                                            <span class="heading-sub-title-alt text-uppercase theme-color-">Multipurpose</span>
                                            <h2 class="text-uppercase">Huge possibilities</h2>
                                        </div>
                                        congue laoreet elit metus eget diam. Proin ac metus diam. In quis scelerisque velit. Proin pellentesque neque ut scelerisque dapibus. Praesent elementum feugiat dignissim. Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque mattis, leo quam aliquet diam, congue laoreet elit metus eget diam. Proin ac metus diam. In quis scelerisque velit. Proin pellentesque neque ut scelerisque dapibus. Praesent elementum feugiat dignissim. Nunc placerat mi id nisi interdum mollis.
                                    </div>
                                    <div id="tab-18" class="tab-pane">
                                        <div class="heading-title-alt">
                                            <span class="heading-sub-title-alt text-uppercase theme-color-">sky is the limit</span>
                                            <h2 class="text-uppercase">we work together for fun</h2>
                                        </div>
                                        Proin ac metus diam. In quis scelerisque velit. Leo quam aliquet diam, congue laoreet elit metus eget diam. Proin pellentesque neque ut scelerisque dapibus. Praesent elementum feugiat dignissim. Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque mattis, leo quam aliquet diam, congue laoreet elit metus eget diam. Proin ac metus diam. In quis scelerisque velit. Proin pellentesque neque ut scelerisque dapibus. Praesent elementum feugiat dignissim. Nunc placerat mi id nisi interdum mollis.
                                    </div>
                                    <div id="tab-19" class="tab-pane">
                                        <div class="heading-title-alt">
                                            <span class="heading-sub-title-alt text-uppercase theme-color-">responsive</span>
                                            <h2 class="text-uppercase">Unlimited shortcode</h2>
                                        </div>
                                        Kusto ut scelerisque mattis, leo quam aliquet diam, congue laoreet elit metus eget diam. Proin ac metus diam. In quis scelerisque velit. Proin pellentesque neque ut scelerisque dapibus. Praesent elementum feugiat dignissim. Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque mattis, leo quam aliquet diam, congue laoreet elit metus eget diam. Proin ac metus diam. In quis scelerisque velit. Proin pellentesque neque ut scelerisque dapibus. Praesent elementum feugiat dignissim. Nunc placerat mi id nisi interdum mollis.
                                    </div>

                                </div>
                            </div>
                        </section>
                        <!--tabs square end-->

                    </div>

                </div>
            </div>
        </div>
        <!--tabs-->

        <!--fun factor-->
        <div class="dark-bg p-tb-50">
            <h3 class="text-center text-uppercase" style="color: #e6e6e6 !important;">Relation with Customers</h3>
            <div class="container">
                <div class="row">
                    <div class=" inline-block">
                        <div class="col-md-3 ">
                            <div class="fun-factor fun-icon-text-parallel alt">
                                <div class="icon">
                                    <i class="icon-layers light-txt"></i>
                                    <h1 class="timer light-txt" data-from="0" data-to="40" data-speed="1000"> </h1>
                                </div>
                                <div class="fun-info light-txt">
                                    <span>HTML multipage</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="fun-factor fun-icon-text-parallel alt">
                                <div class="icon">
                                    <i class="icon-computer_imac_slim light-txt"></i>
                                    <h1 class="timer light-txt" data-from="0" data-to="30" data-speed="1000"> </h1>
                                </div>
                                <div class="fun-info light-txt">
                                    <span>One page demo</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="fun-factor fun-icon-text-parallel alt">
                                <div class="icon">
                                    <i class="icon-database light-txt"></i>
                                    <h1 class="timer light-txt" data-from="0" data-to="87" data-speed="1000"> </h1>
                                </div>
                                <div class="fun-info light-txt">
                                    <span>usefull shortcode</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="fun-factor fun-icon-text-parallel alt">
                                <div class="icon">
                                    <i class="icon-linegraph light-txt"></i>
                                    <h1 class="timer light-txt" data-from="0" data-to="17" data-speed="1000"> </h1>
                                </div>
                                <div class="fun-info light-txt">
                                    <span>Different Category</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--fun factor-->

        <!--testimonial-->
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <h2 class="text-center text-uppercase">Testimonials</h2>
                    <div class="col-md-8 col-md-offset-2">
                        <!--testimonial start-->
                        <div id="testimonial-2" class="">
                            <div class="item">
                                <div class="tst-thumb">
                                    <img class="circle" src="img/site/testimonials/download.jfif" alt="">
                                </div>
                                <div class="content">
                                    <p>
                                        Nam nec dui dolor. Curabitur in laoreet erat. Nam nec dui dolor. Aliquam varius dolor nunc, interdum commodo justoporttitor vitae.
                                        Quisque fermentum purus.
                                    </p>
                                    <div class="testimonial-meta">
                                        - Kevin Paige -
                                        <span>ABC</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="tst-thumb">
                                    <img class="circle" src="img/site/testimonials/download.jfif" alt="">
                                </div>
                                <div class="content">
                                    <p>Vestibulum varius fermentum risus vitae lacinia neque auctor nec. Nunc ac rutrum nulla. Nulla maximus dolor in quam euismod ac viverra libero aliquet. </p>
                                    <div class="testimonial-meta">
                                        - John Doe -
                                        <span>head of marketing, TB</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="tst-thumb">
                                    <img class="circle" src="img/site/testimonials/download.jfif" alt="">
                                </div>
                                <div class="content">
                                    <p>Vestibulum varius fermentum risus vitae lacinia neque auctor nec. Nunc ac rutrum nulla. Nulla maximus dolor in quam euismod ac viverra libero aliquet. </p>
                                    <div class="testimonial-meta">
                                        - Linda Smith -
                                        <span>CEO, TB</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--testimonial end-->
                    </div>
                </div>
            </div>
        </div>
        <!--testimonial-->

        <div class="post-parallax">
            <div class="relative page-content">
                <div class="dark-overlay"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="v-middle">
                                <h3 class="light-txt text-uppercase">Share You Query With Us</h3>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="light-txt space">
                                <h4 class="text-uppercase light-txt">
                                    We are damn expart !
                                </h4>

                                <p class="light-txt">
                                    We are a team of multi-skilled and curious digital specialists who are always up for a massive challenge forever. We are a team of multi-skilled and curious digital specialists who are always up for a massive challenge forever.
                                </p>

                                <div class="p-top-30">

                                    <!-- progress bar start -->
                                    <div class="progress massive-progress">
                                        <!-- <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"> -->
                                            <!-- HTML / CSS / JQUERY  <span>80%</span> -->
                                            <input class="form-control" type="text" placeholder="Your Name">
                                        <!-- </div> -->
                                    </div>
                                    <!-- progress bar end -->

                                    <!-- progress bar start -->
                                    <div class="progress massive-progress">
                                        <!-- <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"> -->
                                            <input class="form-control" type="text" placeholder="Mobile">
                                        <!-- </div> -->
                                    </div>
                                    <!-- progress bar end -->

                                    <!-- progress bar start -->
                                    <div class="text-center">
                                        <!-- <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"> -->
                                            <button type="submit" class="btn rounded" style="padding: 5px 20px; background: #111;">Send</button>
                                        <!-- </div> -->
                                    </div>
                                    <!-- progress bar end -->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr/>

        <!--brands-->
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <h2 class="text-center text-uppercase">Our Top Brands</h2>
                    <div class="col-md-12">
                        <div id="clients-1">
                            <div class="item"><img src="<?php echo base_url("assets/img/site/brands/88315318-creative-logo-letter-design-for-brand-identity-company-profile-or-corporate-logos-with-clean-elegant.jpg"); ?>" alt="Clients" style="height: 100px;"></div>
                            <div class="item"><img src="<?php echo base_url("assets/img/site/brands/attachment_54877373-e1485977528647.jpg"); ?>" alt="Clients" style="height: 100px;"></div>
                            <div class="item"><img src="<?php echo base_url("assets/img/site/brands/clever-hidden-meaning-logo-designs-1.jpg"); ?>" alt="Clients" style="height: 100px;"></div>
                            <div class="item"><img src="<?php echo base_url("assets/img/site/brands/88315318-creative-logo-letter-design-for-brand-identity-company-profile-or-corporate-logos-with-clean-elegant.jpg"); ?>" alt="Clients" style="height: 100px;"></div>
                            <div class="item"><img src="<?php echo base_url("assets/img/site/brands/design-creative-logo-of-any-type.png"); ?>" alt="Clients" style="height: 100px;"></div>
                            <div class="item"><img src="<?php echo base_url("assets/img/site/brands/clever-hidden-meaning-logo-designs-1.jpg"); ?>" alt="Clients" style="height: 100px;"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--brands-->


    </section>
    <!--body content end-->
