    <!--body content start-->
    <section class="body-content">

        <!--team member-->
        <div class="page-content" style="padding-bottom: 0px;">
            <div class="container" style="padding-top: 50px;">
                <div class="row">
                    <div class="m-bot-20 inline-block">
                        <!--title-->
                        <div class="heading-title-alt border-short-bottom text-center">
                            <h3 class="text-uppercase">About Arceur</h3>
                            <div class="half-txt">We are a team of multi-skilled and curious digital specialists who are always up for a challenge and learning as fast as digital is changing. We are a team of multi-skilled and curious digital specialists who are always up for a challenge.
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusamus, pariatur. Hic distinctio, similique quas, beatae veritatis rem dicta minima modi voluptate nam veniam error iste delectus a id, iure aliquam?
                                <br><br>
                                Eius, voluptates beatae autem delectus quisquam omnis, voluptatibus molestias nobis fuga error iusto consequatur dolor corrupti quos ut, mollitia quo labore aspernatur aperiam doloremque. Sapiente blanditiis ad voluptatibus laudantium molestias.
                                Ab repudiandae eligendi nihil laborum quas maxime incidunt ratione porro. Quo at fugiat repellat tempore consequatur ad excepturi nihil nobis iure. Culpa quo nulla iste officia sed, asperiores incidunt delectus.
                            </div>
                        </div>
                        <!--title-->
                    </div>
                </div>
            </div>
        </div>
        <!--team member-->     

        <!--blog-->
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="inline-block">
                        <!--title-->
                        <div class="heading-title-alt border-short-bottom text-center">
                            <h3 class="text-uppercase">SEO Talks</h3>
                            <!-- <div class="half-txt">Sed sit amet iaculis nisi. Mauris ridiculus elementum non felis etewe blandit Pellentesque eu quam sem, ac malesuada leo sem quam pellente.</div> -->
                        </div>
                        <!--title-->
                    </div>


                    <!--blog post-->
                    <div class="post-list-aside">
                        <div class="post-single">
                            <div class="col-md-6">
                                <div class="post-img title-img">
                                    <img src="img/site/seo/professional-workers-500x500.jpg" alt="">
                                    <div class="info">We work together for fun</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="post-desk">
                                    <ul class="post-cat">
                                        <li><a href="#">interface</a></li>
                                        <li><a href="#">standard</a></li>
                                    </ul>
                                    <h4 class="text-uppercase">
                                        <a href="#">we work together for fun</a>
                                    </h4>
                                    <div class="date">
                                        <a href="#" class="author">martin smith</a> july 29, 2015
                                    </div>
                                    <p>
                                        Phasellus fringilla suscipit risus nec eleifend. Pellentesque eu quam sem, ac malesuada leo sem quam pellente. Awesome sliders give you the opportunity to showcase your content.
                                        fringilla suscipit risus nec eleifend. Pellentesque eu quam sem, ac malesuada leo sem quam pellente. Awesome sliders give you the opportunity to showcase your content.
                                    </p>
                                    <a href="#" class="p-read-more">Read More <i class="icon-arrows_slim_right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--blog post-->


                </div>
            </div>
        </div>
        <!--blog-->

        <!--fun factor-->
        <div class="dark-bg p-tb-50">
            <h3 class="text-center text-uppercase" style="color: #e6e6e6 !important;">Relation with Customers</h3>
            <div class="container">
                <div class="row">
                    <div class=" inline-block">
                        <div class="col-md-3 ">
                            <div class="fun-factor fun-icon-text-parallel alt">
                                <div class="icon">
                                    <i class="icon-layers light-txt"></i>
                                    <h1 class="timer light-txt" data-from="0" data-to="40" data-speed="1000"> </h1>
                                </div>
                                <div class="fun-info light-txt">
                                    <span>HTML multipage</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="fun-factor fun-icon-text-parallel alt">
                                <div class="icon">
                                    <i class="icon-computer_imac_slim light-txt"></i>
                                    <h1 class="timer light-txt" data-from="0" data-to="30" data-speed="1000"> </h1>
                                </div>
                                <div class="fun-info light-txt">
                                    <span>One page demo</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="fun-factor fun-icon-text-parallel alt">
                                <div class="icon">
                                    <i class="icon-database light-txt"></i>
                                    <h1 class="timer light-txt" data-from="0" data-to="87" data-speed="1000"> </h1>
                                </div>
                                <div class="fun-info light-txt">
                                    <span>usefull shortcode</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="fun-factor fun-icon-text-parallel alt">
                                <div class="icon">
                                    <i class="icon-linegraph light-txt"></i>
                                    <h1 class="timer light-txt" data-from="0" data-to="17" data-speed="1000"> </h1>
                                </div>
                                <div class="fun-info light-txt">
                                    <span>Different Category</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--fun factor-->

        <!--testimonial-->
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <h2 class="text-center text-uppercase">Testimonials</h2>
                    <div class="col-md-8 col-md-offset-2">
                        <!--testimonial start-->
                        <div id="testimonial-2" class="">
                            <div class="item">
                                <div class="tst-thumb">
                                    <img class="circle" src="img/site/testimonials/download.jfif" alt="">
                                </div>
                                <div class="content">
                                    <p>
                                        Nam nec dui dolor. Curabitur in laoreet erat. Nam nec dui dolor. Aliquam varius dolor nunc, interdum commodo justoporttitor vitae.
                                        Quisque fermentum purus.
                                    </p>
                                    <div class="testimonial-meta">
                                        - Kevin Paige -
                                        <span>ABC</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="tst-thumb">
                                    <img class="circle" src="img/site/testimonials/download.jfif" alt="">
                                </div>
                                <div class="content">
                                    <p>Vestibulum varius fermentum risus vitae lacinia neque auctor nec. Nunc ac rutrum nulla. Nulla maximus dolor in quam euismod ac viverra libero aliquet. </p>
                                    <div class="testimonial-meta">
                                        - John Doe -
                                        <span>head of marketing, TB</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="tst-thumb">
                                    <img class="circle" src="img/site/testimonials/download.jfif" alt="">
                                </div>
                                <div class="content">
                                    <p>Vestibulum varius fermentum risus vitae lacinia neque auctor nec. Nunc ac rutrum nulla. Nulla maximus dolor in quam euismod ac viverra libero aliquet. </p>
                                    <div class="testimonial-meta">
                                        - Linda Smith -
                                        <span>CEO, TB</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--testimonial end-->
                    </div>
                </div>
            </div>
        </div>
        <!--testimonial-->

        <hr/>

        <!--brands-->
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <h2 class="text-center text-uppercase">Our Top Brands</h2>
                    <div class="col-md-12">
                        <div id="clients-1">
                            <div class="item"><img src="img/site/brands/88315318-creative-logo-letter-design-for-brand-identity-company-profile-or-corporate-logos-with-clean-elegant.jpg" alt="Clients" style="height: 100px;"></div>
                            <div class="item"><img src="img/site/brands/attachment_54877373-e1485977528647.jpg" alt="Clients" style="height: 100px;"></div>
                            <div class="item"><img src="img/site/brands/clever-hidden-meaning-logo-designs-1.jpg" alt="Clients" style="height: 100px;"></div>
                            <div class="item"><img src="img/site/brands/88315318-creative-logo-letter-design-for-brand-identity-company-profile-or-corporate-logos-with-clean-elegant.jpg" alt="Clients" style="height: 100px;"></div>
                            <div class="item"><img src="img/site/brands/design-creative-logo-of-any-type.png" alt="Clients" style="height: 100px;"></div>
                            <div class="item"><img src="img/site/brands/clever-hidden-meaning-logo-designs-1.jpg" alt="Clients" style="height: 100px;"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--brands-->

    </section>
    <!--body content end-->
