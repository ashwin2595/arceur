<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="corporate, creative, general, portfolio, photography, blog, e-commerce, shop, product, gallery, retina, responsive">
    <meta name="author" content="Mosaddek">

    <!--favicon icon-->
    <link rel="icon" type="image/png" href="img/favicon.png">

    <title>Arceur</title>

    <!--common style-->


    <link href='http://fonts.googleapis.com/css?family=Abel|Source+Sans+Pro:400,300,300italic,400italic,600,600italic,700,700italic,900,900italic,200italic,200' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url("assets/css/bootstrap.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/font-awesome.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/magnific-popup.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/shortcodes/shortcodes.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/owl.carousel.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/owl.theme.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/style.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/style-responsive.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/default-theme.css") ?>" rel="stylesheet">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>


<!--  preloader start -->
<!-- <div id="tb-preloader">
    <div class="tb-preloader-wave"></div>
</div> -->
<!-- preloader end -->

<div class="wrapper">

    <!--header start-->
    <header id="header" class=" header-full-width  transparent-header ">

        <div class="header-sticky light-header- dark-header header-bottom-border- ">

            <div class="container">
                <div id="massive-menu" class="menuzord">

                    <!--logo start-->
                    <a href="<?php echo base_url(''); ?>" class="logo-brand">
                        <img class="retina" src="img/logo-dark.png" alt=""/>
                    </a>
                    <!--logo end-->

                    <!--mega menu start-->
                    <ul class="menuzord-menu pull-right light">

                        <li class=""><a href="<?php echo base_url(''); ?>">Home</a></li>

                        <li class=""><a href="<?php echo base_url('index.php/services'); ?>">Services</a></li>

                        <li class=""><a href="<?php echo base_url('index.php/gallery'); ?>">Gallery</a></li>

                        <li class=""><a href="<?php echo base_url('index.php/about'); ?>">About Us</a></li>

                        <li class=""><a href="<?php echo base_url('index.php/contact'); ?>">Contact Us</a></li>

                    </ul>
                    <!--mega menu end-->

                </div>
            </div>
        </div>

    </header>
    <!--header end-->