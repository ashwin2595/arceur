<!--body content start-->
<section class="body-content">

        
<div class="page-content">
    <div class="container" style="padding-top: 50px;">
        <div class="row">
            <div class="col-md-12">

                <div class="heading-title-alt text-left ">
                    <h3 class="text-uppercase">Our Gallery</h3>
                    <span class="text-uppercase">Lid est laborum dolo rumes fugats untras.</span>
                </div>

                <!--portfolio carousel-->

                <div id="portfolio-carousel" class=" portfolio-with-title col-3 portfolio-gallery">
                    <div class="portfolio-item">
                        <div class="thumb">
                            <img src="img/site/services/shutterstock_392498833.jpg" alt="">
                            <div class="portfolio-hover">
                                <div class="action-btn">
                                    <a href="img/site/services/shutterstock_392498833.jpg" class="popup-gallery" title="lightbox view"> <i class="icon-basic_magnifier"></i>  </a>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-title">
                            <h4><a href="img/site/services/shutterstock_392498833.jpg" class="popup-gallery2" title="lightbox view">perspiciatis unde omnis</a></h4>
                            <p><a href="#">category 1</a> , <a href="#">category 3</a> </p>
                        </div>
                    </div>

                    <div class="portfolio-item">
                        <div class="thumb">
                            <img src="img/site/services/shutterstock_392498833.jpg" alt="">
                            <div class="portfolio-hover">
                                <div class="action-btn">
                                    <a href="img/site/services/shutterstock_392498833.jpg" class="popup-gallery" title="lightbox view"> <i class="icon-basic_magnifier"></i>  </a>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-title">
                            <h4><a href="img/site/services/shutterstock_392498833.jpg" class="popup-gallery2" title="lightbox view">denouncing pleasure</a></h4>
                            <p><a href="#">category 1</a> , <a href="#">category 3</a> </p>
                        </div>
                    </div>

                    <div class="portfolio-item">
                        <div class="thumb">
                            <img src="img/site/services/shutterstock_392498833.jpg" alt="">
                            <div class="portfolio-hover">
                                <div class="action-btn">
                                    <a href="img/site/services/shutterstock_392498833.jpg" class="popup-gallery" title="lightbox view"> <i class="icon-basic_magnifier"></i>  </a>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-title">
                            <h4><a href="img/site/services/shutterstock_392498833.jpg" class="popup-gallery2" title="lightbox view">annoyances accepted</a></h4>
                            <p><a href="#">category 1</a> , <a href="#">category 3</a> </p>
                        </div>
                    </div>

                    <div class="portfolio-item">
                        <div class="thumb">
                            <img src="img/site/services/shutterstock_392498833.jpg" alt="">
                            <div class="portfolio-hover">
                                <div class="action-btn">
                                    <a href="img/site/services/shutterstock_392498833.jpg" class="popup-gallery" title="lightbox view"> <i class="icon-basic_magnifier"></i>  </a>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-title">
                            <h4><a href="img/site/services/shutterstock_392498833.jpg" class="popup-gallery2" title="lightbox view">annoyances accepted</a></h4>
                            <p><a href="#">category 1</a> , <a href="#">category 3</a> </p>
                        </div>
                    </div>

                    <div class="portfolio-item">
                        <div class="thumb">
                            <img src="img/site/services/shutterstock_392498833.jpg" alt="">
                            <div class="portfolio-hover">
                                <div class="action-btn">
                                    <a href="img/site/services/shutterstock_392498833.jpg" class="popup-gallery" title="lightbox view"> <i class="icon-basic_magnifier"></i>  </a>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-title">
                            <h4><a href="img/site/services/shutterstock_392498833.jpg" class="popup-gallery2" title="lightbox view">annoyances accepted</a></h4>
                            <p><a href="#">category 1</a> , <a href="#">category 3</a> </p>
                        </div>
                    </div>

                    <div class="portfolio-item">
                        <div class="thumb">
                            <img src="img/site/services/shutterstock_392498833.jpg" alt="">
                            <div class="portfolio-hover">
                                <div class="action-btn">
                                    <a href="img/site/services/shutterstock_392498833.jpg" class="popup-gallery" title="lightbox view"> <i class="icon-basic_magnifier"></i>  </a>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-title">
                            <h4><a href="img/site/services/shutterstock_392498833.jpg" class="popup-gallery2" title="lightbox view">annoyances accepted</a></h4>
                            <p><a href="#">category 1</a> , <a href="#">category 3</a> </p>
                        </div>
                    </div>

                </div>

                <!--portfolio carousel-->

            </div>
        </div>
    </div>
</div>

<hr/>

<!--brands-->
<div class="page-content">
    <div class="container">
        <div class="row">
            <h2 class="text-center text-uppercase">Our Top Brands</h2>
            <div class="col-md-12">
                <div id="clients-1">
                    <div class="item"><img src="img/site/brands/88315318-creative-logo-letter-design-for-brand-identity-company-profile-or-corporate-logos-with-clean-elegant.jpg" alt="Clients" style="height: 100px;"></div>
                    <div class="item"><img src="img/site/brands/attachment_54877373-e1485977528647.jpg" alt="Clients" style="height: 100px;"></div>
                    <div class="item"><img src="img/site/brands/clever-hidden-meaning-logo-designs-1.jpg" alt="Clients" style="height: 100px;"></div>
                    <div class="item"><img src="img/site/brands/88315318-creative-logo-letter-design-for-brand-identity-company-profile-or-corporate-logos-with-clean-elegant.jpg" alt="Clients" style="height: 100px;"></div>
                    <div class="item"><img src="img/site/brands/design-creative-logo-of-any-type.png" alt="Clients" style="height: 100px;"></div>
                    <div class="item"><img src="img/site/brands/clever-hidden-meaning-logo-designs-1.jpg" alt="Clients" style="height: 100px;"></div>
                </div>

            </div>
        </div>
    </div>
</div>
<!--brands-->


</section>
<!--body content end-->
