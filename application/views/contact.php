<!--body content start-->
<section class="body-content">

<!--blog-->
<div class="page-content">
    <div class="container">
        <div class="row" style="padding-top: 50px;">
            <div class="inline-block">
                <!--title-->
                <div class="heading-title-alt border-short-bottom text-center">
                    <h2 class="text-uppercase">Contact Us</h2>
                </div>
                <!--title-->
            </div>


            <!--blog post-->
            <div class="post-list-aside text-center">
                <div class="post-single">
                    <div class="col-md-12">
                        <div class="post-img title-img">
                            <iframe class="col-md-12" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14008.112313563905!2d77.20653217196995!3d28.628920553091646!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cfd37b741d057%3A0xcdee88e47393c3f1!2sConnaught%20Place%2C%20New%20Delhi%2C%20Delhi%20110001!5e0!3m2!1sen!2sin!4v1594573003058!5m2!1sen!2sin" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" style="padding: auto 5%;"></iframe>
                        </div>
                    </div>
                    <div class="col-md-12 text-center" style="padding-top: 50px;">
                        <div class="post-desk">
                            <ul class="post-cat">
                                <li><a href="#">India</a></li>
                                <li><a href="#">Delhi</a></li>
                            </ul>
                            <h4 class="text-uppercase">
                                <a href="#">we are here to solve your queries</a>
                            </h4>
                            <div class="date">
                                <a href="#" class="author">Connaught</a> Sector-5
                            </div>
                            <p>
                                Phasellus fringilla suscipit risus nec eleifend. Pellentesque eu quam sem, ac malesuada leo sem quam pellente. Awesome sliders give you the opportunity to showcase your content.
                                fringilla suscipit risus nec eleifend. Pellentesque eu quam sem, ac malesuada leo sem quam pellente. Awesome sliders give you the opportunity to showcase your content.
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <i class="fa fa-phone" style="font-size: 50px;"></i>
                        <h4>Mobile:- +91-1234567890</h4>
                    </div>
                    <div class="col-md-6">
                        <i class="fa fa-envelope" style="font-size: 50px;"></i>
                        <h4>Email:- arceur@gm.com</h4>
                    </div>
                </div>
            </div>
            <!--blog post-->


        </div>
    </div>
</div>
<!--blog-->

<div class="post-parallax">
    <div class="relative page-content">
        <div class="dark-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="v-middle">
                        <h3 class="light-txt text-uppercase">Share You Query With Us</h3>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="light-txt space">
                        <h4 class="text-uppercase light-txt">
                            We are damn expart !
                        </h4>

                        <p class="light-txt">
                            We are a team of multi-skilled and curious digital specialists who are always up for a massive challenge forever. We are a team of multi-skilled and curious digital specialists who are always up for a massive challenge forever.
                        </p>

                        <div class="p-top-30">

                            <!-- progress bar start -->
                            <div class="progress massive-progress">
                                <!-- <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"> -->
                                    <!-- HTML / CSS / JQUERY  <span>80%</span> -->
                                    <input class="form-control" type="text" placeholder="Your Name">
                                <!-- </div> -->
                            </div>
                            <!-- progress bar end -->

                            <!-- progress bar start -->
                            <div class="progress massive-progress">
                                <!-- <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"> -->
                                    <input class="form-control" type="text" placeholder="Mobile">
                                <!-- </div> -->
                            </div>
                            <!-- progress bar end -->

                            <!-- progress bar start -->
                            <div class="text-center">
                                <!-- <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"> -->
                                    <button type="submit" class="btn rounded" style="padding: 5px 20px; background: #111;">Send</button>
                                <!-- </div> -->
                            </div>
                            <!-- progress bar end -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br>
<hr>
<br>


</section>
<!--body content end-->
