    <!--body content start-->
    <section class="body-content">

        <!-- services -->
        <div class="page-content">
            <div class="container" style="padding-top: 50px;">
                <div class="row">
                    <div class="m-bot-20 inline-block">
                        <!--title-->
                        <div class="heading-title-alt border-short-bottom text-center">
                            <h2 class="text-uppercase">Our Services</h2>
                            <div class="half-txt">We are a team of multi-skilled and curious digital specialists who are always up for a challenge and learning as fast as digital is changing. We are a team of multi-skilled and curious digital specialists who are always up for a challenge.</div>
                        </div>
                        <!--title-->
                    </div>
                    <div class="col-md-4">
                        <div class="team-member">
                            <div class="team-img">
                                    <img src="img/site/services/shutterstock_392498833.jpg" alt=""/>
                                <div class="team-intro light-txt">
                                    <h5>Martin Smith</h5>
                                    <span>founder & ceo</span>
                                </div>
                            </div>
                            <div class="team-hover">
                                <div class="desk">
                                    <h4>I love my Studio</h4>
                                    <p>We are a team of multi-skilled and curious digital specialists who are always up for a challenge and learning as fast as digital is changing.</p>
                                </div>
                                <div class="s-link">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="team-member">
                            <div class="team-img">
                                <img src="img/site/services/sx8ewpollcckwslveux3.jpg" alt=""/>
                                <div class="team-intro light-txt">
                                    <h5>Franklin Harbet</h5>
                                    <span>HR Manager</span>
                                </div>
                            </div>
                            <div class="team-hover">
                                <div class="desk">
                                    <h4>Connecting People</h4>
                                    <p>We are a team of multi-skilled and curious digital specialists who are always up for a challenge and learning as fast as digital is changing.</p>
                                </div>
                                <div class="s-link">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="team-member">
                            <div class="team-img">
                                <img src="img/site/services/barandbench_2020-03_b8b8d14c-f8d1-48e5-8aee-cdb702f09b3c_architects.webp" alt=""/>
                                <div class="team-intro light-txt">
                                    <h5>Linda Anderson</h5>
                                    <span>Marketing Manager</span>
                                </div>
                            </div>
                            <div class="team-hover">
                                <div class="desk">
                                    <h4>Network Builder</h4>
                                    <p>We are a team of multi-skilled and curious digital specialists who are always up for a challenge and learning as fast as digital is changing.</p>
                                </div>
                                <div class="s-link">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- services -->  
        
        
        <div class="page-content">
            <div class="container">
                <div class="row">

                    <!--feature box outline start-->

                    <div class="heading-title text-center">
                        <h3 class="text-uppercase">services we provide </h3>
                        <span class="text-uppercase"> Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores </span>
                    </div>

                    <div class="col-md-6">
                        <div class="featured-item feature-outline m-bot-100">
                            <div class="icon theme-color">
                                <i class="icon-lightbulb"></i>
                            </div>
                            <div class="title text-uppercase">
                                <h4>Creative design</h4>
                            </div>
                            <div class="desc">
                                Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="featured-item feature-outline m-bot-100">
                            <div class="icon theme-color">
                                <i class="icon-browser2"></i>
                            </div>
                            <div class="title text-uppercase">
                                <h4>professional code</h4>
                            </div>
                            <div class="desc">
                                Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="featured-item feature-outline m-bot-100">
                            <div class="icon theme-color">
                                <i class="icon-laptop2"></i>
                            </div>
                            <div class="title text-uppercase">
                                <h4>easy customize</h4>
                            </div>
                            <div class="desc">
                                Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="featured-item feature-outline m-bot-100">
                            <div class="icon theme-color">
                                <i class="icon-basic_gear"></i>
                            </div>
                            <div class="title text-uppercase">
                                <h4>24/7 SUPPORT</h4>
                            </div>
                            <div class="desc">
                                Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="featured-item feature-outline">
                            <div class="icon theme-color">
                                <i class="icon-search"></i>
                            </div>
                            <div class="title text-uppercase">
                                <h4>search engine optimization</h4>
                            </div>
                            <div class="desc">
                                Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="featured-item feature-outline">
                            <div class="icon theme-color">
                                <i class="icon-tools2"></i>
                            </div>
                            <div class="title text-uppercase">
                                <h4>web design</h4>
                            </div>
                            <div class="desc">
                                Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend.
                            </div>
                        </div>
                    </div>
                </div>
                <!--feature box outline end-->
            </div>
        </div>

    </section>
    <!--body content end-->
