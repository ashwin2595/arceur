<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Home';

$route['about'] = "home/about";
$route['services'] = "home/services";
$route['gallery'] = "home/gallery";
$route['contact'] = "home/contact";

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;