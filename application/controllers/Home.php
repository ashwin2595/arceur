<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

	public function index(){

        $this->load->view('template/header');
        $this->load->view('index');
        $this->load->view('template/footer');
	}

	public function about(){

        $this->load->view('template/header');
		$this->load->view('about');
        $this->load->view('template/footer');
    }
    
    public function services(){

        $this->load->view('template/header');
		$this->load->view('services');
        $this->load->view('template/footer');
    }
    
    public function gallery(){

        $this->load->view('template/header');
		$this->load->view('gallery');
        $this->load->view('template/footer');
    }
    
    public function contact(){

        $this->load->view('template/header');
		$this->load->view('contact');
        $this->load->view('template/footer');
	}
}
